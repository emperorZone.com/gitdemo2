package com.seecen.gitdemo2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 描述:
 *
 * @author liliang
 * @create 2019-12-31 11:23
 */
@Controller
public class liliangcontroller {
    @GetMapping("/hello")
    public String hello(){
        String word = "hello world";
        return word;
    }
}